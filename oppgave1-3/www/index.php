<?php

require_once 'vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

//when entering localhost as url this will render signUp.html
echo $twig->render('oppgave1.html'); 

