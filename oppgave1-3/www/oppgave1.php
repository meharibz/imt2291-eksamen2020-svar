<?php

require_once 'vendor/autoload.php';
require_once 'classes/DB.php';


$loader = new \Twig\Loader\FilesystemLoader('./twig');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();

//Checks if password is the same both places
if ($_POST['passwordCheck'] != $_POST['password']) {
    $res['status'] = 'FAIL';
    $res['errorMessage'] = 'Password does not match.';
    echo $twig->render('oppgave1.html', $res);
    exit(); 
  }

  //checks if all the fields is set
  if (isset($_POST['email'])&&isset($_POST['password'])&&isset($_POST['firstName'])&&isset($_POST['lastName'])&&isset($_POST['passwordCheck'])) {

    //render errormessage if uername is unvalid
    if (!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/",$_POST['email'])) {
      $res['status'] = 'FAIL';
      $res['errorMessage'] = "Unvalid username/email";
      echo $twig->render('oppgave1.html', $res);
      exit(); 
    }

    //render errormessage if password is to short
    if(strlen($_POST['password'])<8){
      $res['status'] = 'FAIL';
      $res['errorMessage'] = "Password is to short";
      echo $twig->render('oppgave1.html', $res);
      exit();
    }

    //render errormessage if first name is to short
    if(strlen($_POST['firstName'])<1){
      $res['status'] = 'FAIL';
      $res['errorMessage'] = "First name is to short";
      echo $twig->render('oppgave1.html', $res);
      exit();
    }

    //render errormessage if lastname is to short
    if(strlen($_POST['lastName'])<1){
      $res['status'] = 'FAIL';
      $res['errorMessage'] = "Last name is to short";
      echo $twig->render('oppgave1.html', $res);
      exit();
    }

    // Hash password and add user to database, binds and prepare before execute
    $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $sql = 'insert into user (uname, pwd, firstName, lastName) VALUES (?, ?, ?, ?)';
    $sth = $db->prepare($sql);
    $sth->execute (array ($_POST['email'], $_POST['password'], $_POST['firstName'], $_POST['lastName']));


    // if user successfully added, give message to user                      
    if($sth->rowCount()==1) {
    $res['status'] = 'OK';
    $res['message'] = 'You have successfully made a new user!';
    echo $twig->render('oppgave1.html', $res);
    exit();

    //user did not enter the database
    } else {
    $res['status'] = 'FAIL';
    $res['errorMessage'] = 'Failed to insert into contact registry.';
    echo $twig->render('oppgave1.html', $res);
    exit();
    }

    //not all the fields where filled inn
  } else {
    $res['status'] = "FAIL";
    $res['errorMessage'] = "Must fill every field";
    echo $twig->render('oppgave1.html', $res);
    }
 

